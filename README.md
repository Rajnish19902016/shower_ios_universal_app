# README #

This app is one of the simplest app if ever one had made to get current weather condition using yahoo weather api's. 

The app is universal app, ie., user of it can run this app either on iPhone or on an iPad. 

By Default app will show the weather condition of Noida location that is in India.

App has also a textfield on the top having placeholder text "city name" that means to user please enter your or any city name to get that place weather.

After specifying the city name just click on "Get weather" immediately weather list get replaced with the new one - the weather for the city you have specified inside textfield.

Current weather condition is shown at the top just below the textfield area and below that a list having weather condition on coming 10 days.

By clicking on any particular day cell you will be navigated to more detailed information for the day weather.

**Issue that get fixed **

Right now there is one issue on weather screen for current temperature - Weather recorded time is not appear in proper format, just replace the "Created" key with "created" under "ViewController" class for web service json response time will start to appear in correct format(just capital and small 'C' difference).