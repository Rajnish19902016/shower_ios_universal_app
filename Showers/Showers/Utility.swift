//
//  Utility.swift
//  Showers
//
//  Created by Chetu on 5/13/16.
//  Copyright © 2016 Rajnish Tomar. All rights reserved.
//

import UIKit


class Utility : NSObject {
    
    
    
    /**
     Checks for the URL of the particular Document
     - Parameters:
     - N/A
     - returns: NSURL: URL of the document
     
     */
    class func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        print("documentsURL : \(documentsURL)")
        return documentsURL
    }
    
    /**
     Checks the particular file with filename in the documents and returns its path
     - Parameters:
     - filename: Name of the file
     - returns: String: Path of the file
     
     */
    class func fileInDocumentsDirectory(filename: String) -> String {
        
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        print("fileURL.path : \(fileURL.path)")
        return fileURL.path!
        
    }

    
    /**
     Saves the image to document Directory after editing
     - Parameters:
     - image: Image to be saved in the document directory
     - image name: Name of the image
     - returns: N/A
     
     */
    class func saveImageToDocumentDirectory(image : UIImage?, imgName : String){
        
        if let img = image{

            print("img: \(img)")
            print("imgName: \(imgName)")
            let imagePath = fileInDocumentsDirectory("\(imgName).jpeg")
            print("file path :\(imagePath)")
            let fileManager = NSFileManager.defaultManager()
            let imageData: NSData = UIImagePNGRepresentation(img)!
            let status = fileManager.createFileAtPath(imagePath, contents: imageData, attributes: nil)
            print("Status : \(status)")
        }
    }
    
    /**
     Loads the particular image from a directory
     - Parameters:
     - name: Name of the iamge to be loaded
     - success: In case of success, image is loaded and returned
     - failure: In case of failure, error is returned
     - returns: N/A
     
     */
    class func loadImageFromDocumetDirectory(name: String?, success : (image : UIImage) -> (), failure : (error : String) -> ()){
        
        if let imgName = name {
            let imagePath = fileInDocumentsDirectory("\(imgName).jpeg")
            print("Path : \(imagePath)")
            let fileManager = NSFileManager.defaultManager()
            if (fileManager.fileExistsAtPath(imagePath)){
                let imageis: UIImage = UIImage(contentsOfFile: imagePath)!
                success(image: imageis)
            }else{
                print("file path not exists.")
                failure(error: "file path not exists.");
            }
        }
    }
    
}
