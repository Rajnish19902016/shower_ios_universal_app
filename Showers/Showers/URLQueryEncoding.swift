//
//  URLQueryEncoding.swift
//  Showers
//
//  Created by Chetu on 5/12/16.
//  Copyright © 2016 Rajnish. All rights reserved.
//

import Foundation

extension String {
    func URLEncodedString() -> String? {
        let customAllowedSet =  NSCharacterSet.URLQueryAllowedCharacterSet()
        let escapedString = self.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
        return escapedString
    }
}
