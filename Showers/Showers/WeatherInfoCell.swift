//
//  WeatherInfoCell.swift
//  Showers
//
//  Created by Chetu on 5/12/16.
//  Copyright © 2016 Rajnish. All rights reserved.
//

import UIKit

class WeatherInfoCell: UITableViewCell {

    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var weatherImageView: UIImageView!
    
    @IBOutlet weak var maxTempLabel: UILabel!
    
    @IBOutlet weak var minTempLabel: UILabel!
}
