

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var weatherImageView: UIImageView!
    
    
    @IBOutlet weak var weatherTypeLabel: UILabel!
    
    @IBOutlet weak var maxTempLabel: UILabel!
    
    @IBOutlet weak var minTempLabel: UILabel!
    
    @IBOutlet weak var currentTempLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    
    @IBOutlet weak var timeLabel: UILabel!
    
    var weatherIconBaseURL : String =  "https://s.yimg.com/zz/combo?a/i/us/we/52/"
    
    
    
    @IBOutlet weak var weatherInfoTable: UITableView!
    var weatherInfoArray : Array<DayWeatherInfoModal>?
    
    var onTime : String?
    var  currentConditionDict : Dictionary<String, String>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityTextField.delegate = self;
        weatherInfoTable.dataSource = self
        weatherInfoTable.delegate = self;
        self.initiateWheatherRequest(forCity: "Noida");
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: IBAction methods
    
    @IBAction func weatherForecast(sender: UIBarButtonItem) {
        
        if let city = cityTextField.text {
            
            self.currentConditionDict = nil;
            weatherInfoArray?.removeAll();
            self.initiateWheatherRequest(forCity: city);
            
        }
        
    }
    
    
    //MARK: UITextField Delegate method
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        cityTextField.resignFirstResponder();
        return true;
    }
    
    //MARK: UITableView DataSource and Delegate methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return weatherInfoArray?.count ?? 0;
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 0.15*self.view.bounds.size.height;
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! WeatherInfoCell
        
        let dayInfo : DayWeatherInfoModal = weatherInfoArray![indexPath.row]
        
        cell.dayLabel.text = dayInfo.day
        cell.dateLabel.text = dayInfo.date;
        cell.maxTempLabel.text = dayInfo.maxTemp + "°";
        cell.minTempLabel.text = dayInfo.minTemp + "°";
        
        let weatherIconURL = NSURL(string: weatherIconBaseURL + dayInfo.conditionCode + ".gif");
        print(weatherIconURL);
        
        
        Utility.loadImageFromDocumetDirectory(dayInfo.conditionCode,
            success: {[unowned cell](image : UIImage) -> () in
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    cell.weatherImageView.image = image;
                    if indexPath.row == 0 {
                        self.weatherImageView.image = image
                    }
                }
            }, failure: {(errorMessage : String) -> () in
                
                self.getDataFromUrl(weatherIconURL ?? NSURL(string: self.weatherIconBaseURL + "3200.gif")!, completion: {[unowned cell] (data, response, error) -> Void in
                    dispatch_async(dispatch_get_main_queue()) { () -> Void in
                        guard let data = data where error == nil else { return }
                        let img = UIImage(data: data)
                        cell.weatherImageView.image = img
                        if indexPath.row == 0{
                            self.weatherImageView.image = img
                        }
                        Utility.saveImageToDocumentDirectory(img, imgName: dayInfo.conditionCode)
                    }
                })
                
        })
    
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true);
        
        self.performSegueWithIdentifier("Detail", sender: indexPath);
        
        
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let destView : WeatherDetailViewController? = segue.destinationViewController as? WeatherDetailViewController;
        if let viewController = destView {
            let indexPath : NSIndexPath = sender as! NSIndexPath
            let cell  : WeatherInfoCell? = weatherInfoTable.cellForRowAtIndexPath(indexPath) as? WeatherInfoCell;
            if let cel = cell {
                viewController.weatherImage =  cel.weatherImageView.image
                viewController.dayStr = cel.dayLabel.text;
                viewController.dateStr = cel.dateLabel.text;
                viewController.maxTemp = cel.maxTempLabel.text;
                viewController.minTemp = cel.minTempLabel.text;
            }
        }
    }
    
    
    //MARk: Yahoo weather API WebSevice calling helper method
    func initiateWheatherRequest(var forCity name : String){
        
        name = "\" \(name) \""
        let unit = "'C'"
        let limit : Int = 10;
        var wheatherUrL : String = "https://query.yahooapis.com/v1/public/yql?format=json&q=select title,units.temperature, atmosphere , item.condition,item.forecast from weather.forecast where woeid in (select woeid from geo.places where text=\(name)) and u = \(unit) limit \(limit) | sort(field=\"item.forecast.date\", descending=\"false\");"
        wheatherUrL =  wheatherUrL.URLEncodedString()!
        print(wheatherUrL);
        
        let request = NSMutableURLRequest()
        request.URL = NSURL(string: wheatherUrL)!;
        request.HTTPMethod = "GET"
        request.HTTPBody = nil;
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request) {[weak self] (data, response, err) -> Void in
            
            if let resultData = data {
                let result = NSString(data: resultData, encoding: NSUTF8StringEncoding)
                print(result);
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(resultData, options: .MutableLeaves) as? NSDictionary
                    if let dict = json{
                        if let queryDict = dict["query"]{
                            if let resultDict = queryDict["results"] {
                                if let channelArray  = resultDict!["channel"]{
                                    for dataDict in channelArray as! Array<NSDictionary>{
                                        let weatherDict = dataDict["item"];
                                        if let weakSelf = self {
                                            weakSelf.initializeWeatherForDay(weatherDict as? Dictionary<String, Dictionary<String, String>>, forCity : dataDict["title"] as? String, on : queryDict["Created"] as? String);
                                        }
                                    }
                                    if let weakSelf = self{
                                        weakSelf.updateUI()
                                    }
                                }
                            }
                        }
                        
                    }else{
                        
                    }
                }catch let err as NSError {
                    
                    print(err);
                }
                
            }
        }
        task.resume()
    }
    
    //MARK: Helper Method
    
    
    func initializeWeatherForDay(weatherDict : Dictionary<String, Dictionary<String,String>>?,forCity cityName : String?, on timeStr : String?){
        
        
        if self.currentConditionDict == nil && weatherDict != nil{
            self.currentConditionDict = weatherDict!["condition"];
            self.onTime = self.convertDateFormater(timeStr ?? "");
        }
        if let forecast  = weatherDict!["forecast"] {
            let dayWeather = DayWeatherInfoModal(day: forecast["day"], date: forecast["date"], maxTemp: forecast["high"], minTemp: forecast["low"],conditionCode : forecast["code"], text: forecast["text"], city : cityName ?? "")
            if self.weatherInfoArray == nil {
                self.weatherInfoArray = Array();
            }
            
            self.weatherInfoArray?.append(dayWeather);
        }

    }
    
    
    func updateUI(){
        
        dispatch_async(dispatch_get_main_queue()) { () in
            
            
            let obj = self.weatherInfoArray?.first
            self.weatherTypeLabel.text = obj?.text;
            self.maxTempLabel.text = (obj?.maxTemp)! + "°";
            self.minTempLabel.text = (obj?.minTemp)! + "°";
            self.locationLabel.text = (obj?.city)! + "°"
            self.timeLabel.text = self.onTime;
            if let currentCondition = self.currentConditionDict {
                self.currentTempLabel.text =  currentCondition["temp"]! + "°";
            }
            self.weatherInfoTable.reloadData();
        }

    }
    
    /**
    Conver date string format
    - Parameters: Input string of date
    - Return: required date in string format
    */

    func convertDateFormater(datee: String) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = ""//"yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        
        guard let datee = dateFormatter.dateFromString(datee) else {
            assert(false, "no date from string")
            return ""
        }
        
        dateFormatter.dateFormat = "MM/dd/yy, HH:mm a"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let timeStamp = dateFormatter.stringFromDate(datee)
        
        return timeStamp
    }
    
    
    /**
     Gets the data from a URL
     - Parameters:
     - url: URL to get data from
     - completion
     - data: Data
     - response: Response from URL
     - error: Error Message while communication
     - returns: void
     */
    func getDataFromUrl(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
        
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data, response: response, error: error)
            }.resume()
    }


}

