//
//  DayWeatherInfoModal.swift
//  Showers
//
//  Created by Chetu on 5/12/16.
//  Copyright © 2016 Rajnish. All rights reserved.
//

import UIKit

class DayWeatherInfoModal: NSObject {
    
    var day : String
    var date : String
    var maxTemp : String
    var minTemp : String
    var conditionCode : String
    var text : String
    var city : String
    
    
    init(day: String?, date : String? , maxTemp : String?, minTemp : String?, conditionCode : String?,text : String?, city : String?){
        
        self.day = day ?? ""
        self.date = date ?? ""
        self.maxTemp = maxTemp ?? ""
        self.minTemp = minTemp ?? ""
        self.conditionCode = conditionCode ?? "3200"
        self.text = text ?? "Scattered Thunderstorms"//NSURL(string: "http://www.free-icons-download.net/images/cloudy-wear-icon-9916.png")!
        
        let range = city?.rangeOfString("-");
        var cityStr : String?
        if let ct = range{
             cityStr = city?.substringFromIndex(ct.startIndex.successor())
        }
        
        self.city = cityStr ?? "New Delhi, India"
        super.init()
    }
    

}
