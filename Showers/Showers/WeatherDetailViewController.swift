//
//  WeatherDetailViewController.swift
//  Showers
//
//  Created by Chetu on 5/13/16.
//  Copyright © 2016 Rajnish Tomar. All rights reserved.
//

import UIKit

class WeatherDetailViewController: UIViewController {
    
    
    @IBOutlet weak var WeatherConditionImage: UIImageView!
    
    @IBOutlet weak var maxTempLabel: UILabel!

    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var tonightLabel: UILabel!
    
    var weatherImage : UIImage?
    var dayStr : String?
    var dateStr : String?
    var maxTemp : String?
    var minTemp : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let image = weatherImage {
          self.WeatherConditionImage.image = image
        }
        if let min = minTemp {
            minTempLabel.text = min;
            
        }
        if let max = maxTemp {
            maxTempLabel.text = max;
                   }
        if let day = dayStr {
             dayLabel.text = day;
        }
        if let date = dateStr {
            dateLabel.text = date;
            
        }
        
        if (Int(minTemp!) > 15  && Int(maxTemp!) < 40) {
            todayLabel.text = "Today - A safe day to go outside and have some fun or play outdoor games.";
        }else if Int(maxTemp!) > 40{
            todayLabel.text = "Today - A very hot day remain in home with AC on.";
        }else{
            todayLabel.text = "Today - A very cold day remain in home with winter clothes."
        }

        
        // Do any additional setup after loading the view, typically from a nib.
    }

}
